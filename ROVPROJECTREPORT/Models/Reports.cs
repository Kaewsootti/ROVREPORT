﻿using System;
using System.Collections.Generic;

namespace ROVPROJECTREPORT.Models
{
    public partial class Reports
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public string Recreate { get; set; }
        public string Type { get; set; }
    }
}
