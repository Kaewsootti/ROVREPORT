﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ROVPROJECTREPORT.Models;

namespace ROVPROJECTREPORT.Data
{
    public class ROVPROJECTREPORTContext : DbContext
    {
        public ROVPROJECTREPORTContext (DbContextOptions<ROVPROJECTREPORTContext> options)
            : base(options)
        {
        }

        public DbSet<ROVPROJECTREPORT.Models.Reports> Reports { get; set; }
    }
}
